var a;
var b;
var operacion;

function init(){
    var resultado = document.getElementById("result");
    //Números
    var nueve = document.getElementById("nueve");
    var ocho = document.getElementById("ocho");
    var siete = document.getElementById("siete");
    var seis = document.getElementById("seis");
    var cinco = document.getElementById("cinco");
    var cuatro = document.getElementById("cuatro");
    var tres = document.getElementById("tres");
    var dos = document.getElementById("dos");
    var uno = document.getElementById("uno");
    var cero = document.getElementById("cero");

    //Operaciones
    var resta = document.getElementById("resta");
    var suma = document.getElementById("suma");
    var multiplicacion = document.getElementById("multiplicacion");
    var division = document.getElementById("division");
    //.
    var igual= document.getElementById("igual");
    var clr = document.getElementById("clear");

    nueve.onclick = function(e){
        resultado.textContent = resultado.textContent + "9";
    }
    ocho.onclick = function(e){
        resultado.textContent = resultado.textContent + "8";
    }
    siete.onclick = function(e){
        resultado.textContent = resultado.textContent + "7";
    }
    seis.onclick = function(e){
        resultado.textContent = resultado.textContent + "6";
    }
    cinco.onclick = function(e){
        resultado.textContent = resultado.textContent + "5";
    }
    cuatro.onclick = function(e){
        resultado.textContent = resultado.textContent + "4";
    }
    tres.onclick = function(e){
        resultado.textContent = resultado.textContent + "3";
    }
    dos.onclick = function(e){
        resultado.textContent = resultado.textContent + "2";
    }
    uno.onclick = function(e){
        resultado.textContent = resultado.textContent + "1";
    }
    cero.onclick = function(e){
        resultado.textContent = resultado.textContent + "0";
    }
    clr.onclick = function(e){
        resultado.textContent = "";
    }
    suma.onclick = function(e){
        a = resultado.textContent;
        operacion = "+";
        limpiar();
    }
    resta.onclick = function(e){
        a = resultado.textContent;
        operacion = "-";
        limpiar();
    }
    multiplicacion.onclick = function(e){
        a = resultado.textContent;
        operacion = "*";
        limpiar();
    }
    division.onclick = function(e){
        a = resultado.textContent;
        operacion = "/";
        limpiar();
    }
    igual.onclick = function(e){
        b = resultado.textContent;
        resolver();
    }

    document.onkeydown = function(e){
        var move = false;
        
        if(e.keyCode == 96){
            resultado.textContent = resultado.textContent + "0";
        }
        if(e.keyCode == 97){
            resultado.textContent = resultado.textContent + "1";
        }
        if(e.keyCode == 98){
            resultado.textContent = resultado.textContent + "2";
        }
        if(e.keyCode == 99){
            resultado.textContent = resultado.textContent + "3";
        }
        if(e.keyCode == 100){
            resultado.textContent = resultado.textContent + "4";
        }
        if(e.keyCode == 101){
            resultado.textContent = resultado.textContent + "5";
        }
        if(e.keyCode == 102){
            resultado.textContent = resultado.textContent + "6";
        }
        if(e.keyCode == 103){
            resultado.textContent = resultado.textContent + "7";
        }
        if(e.keyCode == 104){
            resultado.textContent = resultado.textContent + "8";
        }
        if(e.keyCode == 105){
            resultado.textContent = resultado.textContent + "9";
        }
        if(e.keyCode == 67){
            limpiar();
        }
        if(e.keyCode == 106){
            a = resultado.textContent;
            operacion = "*";
            limpiar();
        }
        if(e.keyCode == 107){
            a = resultado.textContent;
            operacion = "+";
            limpiar();
        }
        if(e.keyCode == 109){
            a = resultado.textContent;
            operacion = "-";
            limpiar();
        }
        if(e.keyCode == 111){
            a = resultado.textContent;
            operacion = "/";
            limpiar();
        }
        if(e.keyCode == 13){
            b = resultado.textContent;
            resolver();
        }
    }
    function limpiar(){
        resultado.textContent = "";
    }
    function resolver(){
        var res = 0;
        switch(operacion){
          case "+":
            res = parseFloat(a) + parseFloat(b);
            break;
          case "-":
              res = parseFloat(a) - parseFloat(b);
              break;
          case "*":
            if(a != 0 && b !=0){
                res = parseFloat(a) * parseFloat(b);
            }else{
                alert("No debe multiplicar por 0");
                resultado.textContent = "";
            }
            break;
          case "/":
            if(a != 0 && b !=0){
                res = parseFloat(a) / parseFloat(b);
            }else{
                alert("No debe dividir por 0");
                resultado.textContent = "";
            }
            break;
        }
        resultado.textContent = res;
    }
}


