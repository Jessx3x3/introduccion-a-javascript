
var value_1 = document.getElementById("number_1");
var value_2 = document.getElementById("number_2");
var randomNumber = document.getElementById("random");
var btn = document.getElementById("btn_send");

btn.addEventListener("click", function(){

    var n1 = parseInt(value_1.value);
    var n2 = parseInt(value_2.value);

    if(isNaN(n1) || isNaN(n2)){
        alert("Debe ingresar números");
    }else{
        randomNumber.innerHTML = getRnd(n1,n2);
    }
})

function getRnd(min, max) {
    return parseInt(Math.floor(Math.random() * max) + min);
}
